/**
 * @addtogroup Group
 * @{
 * @file ListMapTest.c
 * @author Massimiliano Pagani
 * @version 1.0
 * @date 2024-07-02
 *
 */

#include <stdio.h>
#include <List.h>
#include <stdlib.h>

static inline void int_dtor( int* x )
{
    (void)x;
}

static inline void char_dtor( char* x )
{
    (void)x;
}

DEFINE_LIST(int)
DEFINE_LIST(char)

#define lambda(return_type, function_body) \
  ({ \
    return_type anon_func_name_ function_body \
    anon_func_name_; \
  })

int main()
{
   List_int* a = ListInit( int, { 1, 2, 3, 4 } );
   ListForeach( a, lambda( void, (int const* x){ printf( "%d\n", *x );} ));

   List_char* b = ListMap( char, a, lambda(char, (int const* x){ return 'A'+*x; }) );
   ListForeach( b, lambda( void, (char const* x){ printf( "%c\n", *x );} ));
}

///@}


