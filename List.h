/**
 * @addtogroup Group
 * @{
 * @file List.h
 * @author Massimiliano Pagani
 * @version 1.0
 * @date 2024-07-02
 *
 */

#if !defined( LIST_H )
#define LIST_H

#include <stdbool.h>

#define DEFINE_LIST(T)                                          \
    typedef struct List_##T                                     \
    {                                                           \
        struct List_##T* next;                                  \
        T data;                                                 \
    }                                                           \
    List_##T;                                                   \
                                                                \
    typedef T List_##T##_Type;                                  \
                                                                \
    static inline T const* List_##T##_getData( List_##T const* n )     \
    {                                                           \
        return &n->data;                                        \
    }                                                           \
                                                                \
    static inline List_##T const* List_##T##_getNext( List_##T const* n )     \
    {                                                           \
        return n->next;                                         \
    }                                                           \
                                                                \
    static inline List_##T* List_##T##_new( bool (*construct)(T*) )   \
    {                                                           \
        List_##T * node = malloc( sizeof( List_##T ));          \
        if( node == NULL )                                      \
        {                                                       \
            return NULL;                                        \
        }                                                       \
        if( !construct( &node->data ))                          \
        {                                                       \
            free( node );                                       \
            return NULL;                                        \
        }                                                       \
        node->next = NULL;                                      \
        return node;                                            \
    }                                                           \
                                                                \
    static inline void List_##T##_delete( List_##T* list )      \
    {                                                           \
        while( list != NULL )                                   \
        {                                                       \
            List_##T* current = list;                           \
            list = list->next;                                  \
            T##_dtor( &current->data );                         \
            free( current );                                    \
        }                                                       \
    }
                                                                
 
#define ListInit( T, ... )                                      \
    ({                                                          \
        typedef List_##T Node;                                  \
        T source[] = __VA_ARGS__;                               \
        size_t const Size = sizeof(source)/sizeof(T);           \
        Node* head = NULL;                                      \
        Node** prev = &head;                                    \
        for( size_t i=0; i != Size; ++i )                       \
        {                                                       \
            bool move( T* dest ) { *dest = source[i]; return true; };   \
            *prev = List_##T##_new( move );                     \
            if( *prev == NULL )                                 \
            {                                                   \
                /* destroy everything & return */               \
                List_##T##_delete( head );                      \
                head = NULL;                                    \
                break;                                          \
            }                                                   \
            prev = &(*prev)->next;                              \
        }                                                       \
        head;                                                   \
    })

#define ListForeach( L, Fn )                                    \
    {                                                           \
        for( typeof(L) scan = (L);                              \
            scan != NULL;                                       \
            scan = scan->next )                                 \
        {                                                       \
            Fn( &scan->data );                                  \
        }                                                       \
    }

#define List_new(X)


#define ListMap( T, L, Fn )                                     \
    ({                                                          \
        typedef typeof(*(L)) Lf;                                \
        typedef List_##T Lt;                                    \
        Lt* head = NULL;                                        \
        Lt** prev = &head;                                      \
        Lf const* scan = L;                                     \
        for( ; scan != NULL; scan = scan->next )                \
        {                                                       \
            bool f( T* p ) {                                    \
                *p = Fn( &scan->data );                         \
                return true;                                    \
            };                                                  \
            *prev = List_##T##_new( f );                        \
            if( *prev == NULL )                                 \
            {                                                   \
                /* destroy everything & return */               \
                head = NULL;                                    \
                break;                                          \
            }                                                   \
            prev = &(*prev)->next;                              \
        }                                                       \
        head;                                                   \
    })


#endif
///@}


