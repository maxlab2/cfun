/**
 * @addtogroup Group
 * @{
 * @file Either.h
 * @author Massimiliano Pagani
 * @version 1.0
 * @date 2024-07-03
 *
 */


#if !defined( EITHER_H )
#define EITHER_H

#include <stdbool.h>

#define DEFINE_EITHER(L,R)                                      \
    typedef struct                                              \
    {                                                           \
        bool isRight;                                           \
        union {                                                 \
            Left left;                                          \
            Right right;                                        \
        };                                                      \
    }                                                           \
    Either_##L##_##R;                                           \
                                                                \
    static inline void Either_##L##_##R##_moveCtorLeft(         \
        Either_##L##_##R* either,                               \
        L left                                                  \
    )                                                           \
    {                                                           \
        either->isRight = false;                                \
        either->left = left;                                    \
    }                                                           \
                                                                \
    static inline void Either_##L##_##R##_copyCtorLeft(         \
        Either_##L##_##R* either,                               \
        L left                                                  \
    )                                                           \
    {                                                           \
        either->isRight = false;                                \
        L##_copyCtor( &either->left, &left );                   \
    }                                                           \
                                                                \
    static inline void Either_##L##_##R##_moveCtoRight(         \
        Either_##L##_##R* either,                               \
        R right                                                 \
    )                                                           \
    {                                                           \
        either->isRight = true;                                 \
        either->right = right;                                  \
    }                                                           \
                                                                \
    static inline void Either_##L##_##R##_copyCtoRight(         \
        Either_##L##_##R* either,                               \
        R right                                                 \
    )                                                           \
    {                                                           \
        either->isRight = true;                                 \
        R##_copyCtor( &either->right, &right );                 \
    }                                                           \
                                                                \
    static inline R const* Either_##L##_##R##_getLeft(          \
        Either_##L##_##R const* either                          \
    )                                                           \
    {                                                           \
        assert( !either->isRight );                             \
        return &either->left;                                   \
    }                                                           \
                                                                \
    static inline R const* Either_##L##_##R##_getRight(         \
        Either_##L##_##R const* either                          \
    )                                                           \
    {                                                           \
        assert( either->isRight );                              \
        return &either->right;                                  \
    }

#define EitherMap( E, MapF )                                    \
    ({                                                          \
        RT? result;                                             \
        if( (E).isRight )                                       \
        {                                                       \
            RT?_moveCtorRight( &result, (MapF)(  &(E)->right ));  \
        }                                                       \
        else                                                    \
        {                                                       \
            RT?_copyCtorLeft( &result, &(E)->left ));     \
        }                                                       \
        result;                                                 \
    })


#endif
///@}


